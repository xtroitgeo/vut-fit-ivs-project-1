# IVS - Practical aspects of software development
# Project 1
[Assignment](task.pdf)  in Czech.

## Project has 3 tasks
### 1st task details
- Black Box Testing
- Test basic operations on an unknown implementation of Red-Black tree based on their definition

### 2nd task details
- White Box Testing
- Test defined operations on matrices based on their known implementation

### 3rd task details
- Test Driven Development (TDD)
- Simple implementation of a priority queue based on a single linked list that allows you to insert a new item
  (at any position) without moving other items
- Implementation conforms to the specification and passes predefined set of tests

  
## Evaluation 16,5/18
