//======== Copyright (c) 2021, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Test Driven Development - priority queue code
//
// $NoKeywords: $ivs_project_1 $tdd_code.cpp
// $Author:     Georgii Troitckii <xtroit00@stud.fit.vutbr.cz>
// $Date:       $2022-02-21
//============================================================================//
/**
 * @file tdd_code.cpp
 * @author Georgii Troitckii
 * 
 * @brief Implementace metod tridy prioritni fronty.
 */

#include <stdlib.h>
#include <stdio.h>

#include "tdd_code.h"

//============================================================================//
// ** ZDE DOPLNTE IMPLEMENTACI **
//
// Zde doplnte implementaci verejneho rozhrani prioritni fronty (Priority Queue)
// 1. Verejne rozhrani fronty specifikovane v: tdd_code.h (sekce "public:")
//    - Konstruktor (PriorityQueue()), Destruktor (~PriorityQueue())
//    - Metody Insert/Remove/Find/GetHead ...
//    - Pripadne vase metody definovane v tdd_code.h (sekce "protected:")
//
// Cilem je dosahnout plne funkcni implementace prioritni fronty implementovane
// pomoci tzv. "singly linked list", ktera bude splnovat dodane testy
// (tdd_tests.cpp).
//============================================================================//

PriorityQueue::PriorityQueue(): queue_length(0), m_pHead(nullptr){}

PriorityQueue::~PriorityQueue()
{
    while (m_pHead != nullptr){
        Remove(m_pHead->value);
    }
}

void PriorityQueue::Insert(int value)
{
    Element_t* new_node = CreateNewNode(value);

    /// Insert into empty queue
    if (m_pHead == nullptr) {
        m_pHead = new_node;
    }
    else if (value > m_pHead->value) {  /// Insert on first pos
        new_node->pNext = m_pHead;
        m_pHead = new_node;
    }
    else {
        Element_t* iter = m_pHead;
        Element_t* prev;

        /// Insert on current pos based on value
        while (iter != nullptr) {
            if (value >= iter->value) {
                prev->pNext = new_node;
                new_node->pNext = iter;
                break;
            }
            prev = iter;
            iter = iter->pNext;
        }
        /// Push back
        if (iter == nullptr) {
            prev->pNext = new_node;
        }
    }
    queue_length++;
}

PriorityQueue::Element_t* PriorityQueue::CreateNewNode(int value)
{
    Element_t* new_node = new Element_t;
    new_node->value = value;
    new_node->pNext = nullptr;
    return new_node;
}

bool PriorityQueue::Remove(int value)
{
    Element_t* searched_node = m_pHead;
    Element_t* prev = nullptr;

    while (searched_node != nullptr){
        if (searched_node->value == value){

            if (searched_node == m_pHead){
                m_pHead = searched_node->pNext;
                searched_node->pNext = nullptr;
                delete searched_node;
            }
            else if (searched_node->pNext == nullptr){
                prev->pNext = nullptr;
                delete searched_node;
            }
            else {
                prev->pNext = searched_node->pNext;
                searched_node->pNext = nullptr;
                delete searched_node;
            }
            queue_length--;
            return true;
        }
        prev = searched_node;
        searched_node = searched_node->pNext;
    }
    return false;
}

PriorityQueue::Element_t *PriorityQueue::Find(int value)
{
    Element_t* iter = m_pHead;

    while (iter != nullptr){
        if (iter->value == value){
            return iter;
        }
        iter = iter->pNext;
    }
    return nullptr;
}

size_t PriorityQueue::Length()
{
	return queue_length;
}

PriorityQueue::Element_t *PriorityQueue::GetHead()
{
    return m_pHead;
}

/*** Konec souboru tdd_code.cpp ***/
