//======== Copyright (c) 2021, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     White Box - Tests suite
//
// $NoKeywords: $ivs_project_1 $white_box_code.cpp
// $Author:     Georgii Troitckii <xtroit00@stud.fit.vutbr.cz>
// $Date:       $2022-02-21
//============================================================================//
/**
 * @file white_box_tests.cpp
 * @author Georgii Troitckii
 * 
 * @brief Implementace testu prace s maticemi.
 */

#include "gtest/gtest.h"
#include "white_box_code.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy operaci nad maticemi. Cilem testovani je:
// 1. Dosahnout maximalniho pokryti kodu (white_box_code.cpp) testy.
// 2. Overit spravne chovani operaci nad maticemi v zavislosti na rozmerech 
//    matic.
//============================================================================//

/// @brief Tests for matrix 1x1
class Matrix_1x1 : public ::testing::Test
{
protected:
    Matrix matrix1;
};

TEST_F(Matrix_1x1, Constructor)
{
    EXPECT_EQ(matrix1.get(0, 0), 0);
    Matrix m1(1, 1);
    EXPECT_EQ(m1.get(0,0), 0);
}

TEST_F(Matrix_1x1, Setter)
{
    matrix1.set(0, 0, 6);
    EXPECT_EQ(matrix1.get(0, 0), 6);

    matrix1.set(1, 1, 9);
    EXPECT_ANY_THROW(matrix1.get(1, 1));
}

TEST_F(Matrix_1x1, Getter)
{
    EXPECT_ANY_THROW(matrix1.get(1, 1));
}

TEST_F(Matrix_1x1, EqualOperator)
{
    Matrix matrix2;
    matrix2.set(0, 0, 5);
    matrix1.set(0, 0, 5);
    EXPECT_TRUE(matrix1 == matrix2);

    matrix1.set(0, 0, -5);
    EXPECT_FALSE(matrix1 == matrix2);
}

TEST_F(Matrix_1x1, SummOperator)
{
    Matrix matrix2;
    matrix2.set(0, 0, -5);
    matrix1.set(0, 0, 5);
    Matrix m_result = matrix1 + matrix2;
    EXPECT_EQ(m_result.get(0, 0), 0);
}

TEST_F(Matrix_1x1, MultOperator)
{
    matrix1.set(0, 0, 10);
    Matrix m_result = matrix1 * 10;
    EXPECT_EQ(m_result.get(0, 0), 100);

    Matrix matrix2;
    matrix2.set(0, 0, 2.5);
    m_result = matrix1 * matrix2;
    EXPECT_EQ(m_result.get(0, 0), 25);
}

TEST_F(Matrix_1x1, SolveEquation)
{
    matrix1.set(0, 0, 10);
    auto result = matrix1.solveEquation({5});
    EXPECT_EQ(result.at(0), 0.5);
    EXPECT_ANY_THROW(matrix1.solveEquation({6, 5}));
}

TEST_F(Matrix_1x1, Transpose)
{
    matrix1.set(0, 0, 21.2);
    Matrix result_matrix = matrix1.transpose();
    EXPECT_EQ(result_matrix.get(0, 0), 21.2);
}

TEST_F(Matrix_1x1, Inverse)
{
    matrix1.set(0, 0, 21.2);
    EXPECT_ANY_THROW(matrix1.inverse());
}

///============================================================================///

/// @brief Tests for matrix 2x2
class Matrix_2x2 : public ::testing::Test
{
protected:

    double round_with_precision( double val )
    {
        std::stringstream stringstream;
        stringstream << std::setprecision(1) << std::fixed << val;
        double new_val = stod(stringstream.str());
        return new_val;
    }

    Matrix matrix1{2,2};
};

TEST_F(Matrix_2x2, Constructor)
{
    Matrix m2(2, 2);
    EXPECT_EQ(m2.get(0,0), 0);
    EXPECT_EQ(m2.get(0,1), 0);
    EXPECT_EQ(m2.get(1,0), 0);
    EXPECT_EQ(m2.get(1,1), 0);
}

TEST_F(Matrix_2x2, Setter)
{
    matrix1.set(1, 0, -2);
    EXPECT_EQ(matrix1.get(1, 0), -2);
    EXPECT_EQ(matrix1.get(0, 0), 0);

    matrix1.set({{5,7}, {9,2}});
    EXPECT_EQ(matrix1.get(0, 0), 5);
    EXPECT_EQ(matrix1.get(0, 1), 7);
    EXPECT_EQ(matrix1.get(1, 0), 9);
    EXPECT_EQ(matrix1.get(1, 1), 2);

    EXPECT_FALSE(matrix1.set({{5}}));
}

TEST_F(Matrix_2x2, Getter)
{
    EXPECT_EQ(matrix1.get(1, 1), 0);
    EXPECT_ANY_THROW(matrix1.get(2, 2));
}

TEST_F(Matrix_2x2, EqualOperator)
{
    Matrix matrix2(2,2);
    matrix2.set({{1,2}, {3,4}});
    matrix1.set({{1,2}, {3,4}});
    EXPECT_TRUE(matrix1 == matrix2);

    matrix1.set({{1,2}, {3,0}});
    EXPECT_FALSE(matrix1 == matrix2);

    Matrix matrix3(1,2);
    matrix3.set({{2,2}});
    EXPECT_ANY_THROW(matrix1 == matrix3);
}

TEST_F(Matrix_2x2, SummOperator)
{
    Matrix matrix2(2, 2);
    matrix2.set({{-1,2}, {-3,4}});
    matrix1.set({{1,2}, {3,4}});
    Matrix m_result = matrix1 + matrix2;
    EXPECT_EQ(m_result.get(0, 0), 0);
    EXPECT_EQ(m_result.get(0, 1), 4);
    EXPECT_EQ(m_result.get(1, 0), 0);
    EXPECT_EQ(m_result.get(1, 1), 8);

    Matrix matrix3;
    EXPECT_ANY_THROW(matrix1 + matrix3);
}

TEST_F(Matrix_2x2, MultOperator)
{
    matrix1.set({{-1,2}, {-3,4}});
    Matrix m_result = matrix1 * (-1);
    EXPECT_EQ(m_result.get(0, 0), 1);
    EXPECT_EQ(m_result.get(0, 1), -2);
    EXPECT_EQ(m_result.get(1, 0), 3);
    EXPECT_EQ(m_result.get(1, 1), -4);

    Matrix matrix2(1,2);
    matrix2.set({{10, 10}});
    m_result = matrix2 * matrix1;
    EXPECT_EQ(m_result.get(0, 0), -40);
    EXPECT_EQ(m_result.get(0, 1), 60);
    EXPECT_ANY_THROW(matrix1 * matrix2);

    Matrix matrix3(2,2);
    matrix3.set({{5,20}, {-100,3}});
    m_result = matrix3 * matrix1;
    EXPECT_EQ(m_result.get(0, 0), -65);
    EXPECT_EQ(m_result.get(0, 1), 90);
    EXPECT_EQ(m_result.get(1, 0), 91);
    EXPECT_EQ(m_result.get(1, 1), -188);

}

TEST_F(Matrix_2x2, SolveEquation)
{
    matrix1.set({{-9,3}, {-2,1}});
    auto result = matrix1.solveEquation({1,0});
    EXPECT_EQ(Matrix_2x2::round_with_precision(result.at(0)), -0.3);
    EXPECT_EQ(Matrix_2x2::round_with_precision(result.at(1)), -0.7);
    EXPECT_ANY_THROW(matrix1.solveEquation({1, 2, 3}));

    matrix1.set({{1,2}, {2,4}});
    EXPECT_ANY_THROW(matrix1.solveEquation({1, 2}));
}

TEST_F(Matrix_2x2, Transpose)
{
    matrix1.set({{3,5}, {12,0}});
    Matrix result_matrix = matrix1.transpose();
    EXPECT_EQ(result_matrix.get(0, 0), 3);
    EXPECT_EQ(result_matrix.get(0, 1), 12);
    EXPECT_EQ(result_matrix.get(1, 0), 5);
    EXPECT_EQ(result_matrix.get(1, 1), 0);
}

TEST_F(Matrix_2x2, Inverse)
{
    matrix1.set({{3,5}, {12,0}});
    Matrix result_matrix = matrix1.inverse();

    EXPECT_EQ(Matrix_2x2::round_with_precision(result_matrix.get(0, 0)), 0);
    EXPECT_EQ(Matrix_2x2::round_with_precision(result_matrix.get(0, 1)), 0.1);
    EXPECT_EQ(Matrix_2x2::round_with_precision(result_matrix.get(1, 0)), 0.2);
    EXPECT_EQ(Matrix_2x2::round_with_precision(result_matrix.get(1, 1)), -0.1);

    matrix1.set({{1,2}, {2,4}});
    EXPECT_ANY_THROW(matrix1.inverse());
}

///============================================================================///

/// @brief Tests for matrix 3x3
class Matrix_3x3 : public ::testing::Test
{
protected:

    virtual void SetUp() {
        matrix1.set({{5,7,10}, {9,2,1}, {9,8,2}});
    }

    double round_with_precision( double val )
    {
        std::stringstream stringstream;
        stringstream << std::setprecision(1) << std::fixed << val;
        double new_val = stod(stringstream.str());
        return new_val;
    }

    Matrix matrix1{3,3};
};

TEST_F(Matrix_3x3, Constructor)
{
    Matrix m2(3, 3);
    EXPECT_EQ(m2.get(0,0), 0);
    EXPECT_EQ(m2.get(0,1), 0);
    EXPECT_EQ(m2.get(0,2), 0);
    EXPECT_EQ(m2.get(1,0), 0);
    EXPECT_EQ(m2.get(1,1), 0);
    EXPECT_EQ(m2.get(1,2), 0);
    EXPECT_EQ(m2.get(2,0), 0);
    EXPECT_EQ(m2.get(2,1), 0);
    EXPECT_EQ(m2.get(2,2), 0);
}

TEST_F(Matrix_3x3, Setter)
{
    matrix1.set(2, 1, 100);
    EXPECT_EQ(matrix1.get(2, 1), 100);

    EXPECT_EQ(matrix1.get(0,0), 5);
    EXPECT_EQ(matrix1.get(0,1), 7);
    EXPECT_EQ(matrix1.get(0,2), 10);
    EXPECT_EQ(matrix1.get(1,0), 9);
    EXPECT_EQ(matrix1.get(1,1), 2);
    EXPECT_EQ(matrix1.get(1,2), 1);
    EXPECT_EQ(matrix1.get(2,0), 9);
    EXPECT_EQ(matrix1.get(2,1), 100);
    EXPECT_EQ(matrix1.get(2,2), 2);
}

TEST_F(Matrix_3x3, Getter)
{
    EXPECT_EQ(matrix1.get(2, 2), 2);
    EXPECT_ANY_THROW(matrix1.get(10, 1));
}

TEST_F(Matrix_3x3, EqualOperator)
{
    Matrix matrix2(3,3);
    matrix2.set({{5,7,10}, {9,2,1}, {9,8,2}});
    EXPECT_TRUE(matrix1 == matrix2);

    matrix1.set({{1,1,1}, {2,2,2}, {3,3,3}});
    EXPECT_FALSE(matrix1 == matrix2);

    Matrix matrix3(1,2);
    matrix3.set({{2,2}});
    EXPECT_ANY_THROW(matrix1 == matrix3);
}

TEST_F(Matrix_3x3, SummOperator)
{
    Matrix matrix2(3, 3);
    matrix2.set({{5,7,10}, {9,2,1}, {9,8,2}});

    Matrix m_result = matrix1 + matrix2;
    EXPECT_EQ(m_result.get(0,0), 10);
    EXPECT_EQ(m_result.get(0,1), 14);
    EXPECT_EQ(m_result.get(0,2), 20);
    EXPECT_EQ(m_result.get(1,0), 18);
    EXPECT_EQ(m_result.get(1,1), 4);
    EXPECT_EQ(m_result.get(1,2), 2);
    EXPECT_EQ(m_result.get(2,0), 18);
    EXPECT_EQ(m_result.get(2,1), 16);
    EXPECT_EQ(m_result.get(2,2), 4);

    Matrix matrix3;
    EXPECT_ANY_THROW(matrix1 + matrix3);
}

TEST_F(Matrix_3x3, MultOperator)
{
    matrix1.set({{1,2,3}, {4,5,6}, {7,8,9}});
    Matrix m_result = matrix1 * (4);
    EXPECT_EQ(m_result.get(0,0), 4);
    EXPECT_EQ(m_result.get(0,1), 8);
    EXPECT_EQ(m_result.get(0,2), 12);
    EXPECT_EQ(m_result.get(1,0), 16);
    EXPECT_EQ(m_result.get(1,1), 20);
    EXPECT_EQ(m_result.get(1,2), 24);
    EXPECT_EQ(m_result.get(2,0), 28);
    EXPECT_EQ(m_result.get(2,1), 32);
    EXPECT_EQ(m_result.get(2,2), 36);


    Matrix matrix2(3,2);
    matrix2.set({{0, 2.5}, {0.5, 3}, {4.5, 5}});
    m_result = matrix1 * matrix2;
    EXPECT_EQ(m_result.get(1, 1), 55);
    EXPECT_EQ(m_result.get(2, 1), 173.0/2);
    EXPECT_ANY_THROW(matrix2 * matrix1);
}

TEST_F(Matrix_3x3, SolveEquation)
{

    matrix1.set({{-9,3,2}, {-2,1,1}, {2,2,2}});
    auto result = matrix1.solveEquation({1,0,2});
    EXPECT_EQ(Matrix_3x3::round_with_precision(result.at(0)), 0.3);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result.at(1)), 2.7);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result.at(2)), -2);
    EXPECT_ANY_THROW(matrix1.solveEquation({1}));
    EXPECT_ANY_THROW(matrix1.solveEquation({1,2,3,4}));

    matrix1.set({{1,1,1}, {2,2,2},{10,10,10}});
    EXPECT_ANY_THROW(matrix1.solveEquation({1, 2, 3}));
}

TEST_F(Matrix_3x3, Transpose)
{
    matrix1.set({{-9,3,2}, {-2,1,1}, {2,2,2}});
    Matrix result_matrix = matrix1.transpose();
    EXPECT_EQ(result_matrix.get(0,0), -9);
    EXPECT_EQ(result_matrix.get(0,1), -2);
    EXPECT_EQ(result_matrix.get(0,2), 2);
    EXPECT_EQ(result_matrix.get(1,0), 3);
    EXPECT_EQ(result_matrix.get(1,1), 1);
    EXPECT_EQ(result_matrix.get(1,2), 2);
    EXPECT_EQ(result_matrix.get(2,0), 2);
    EXPECT_EQ(result_matrix.get(2,1), 1);
    EXPECT_EQ(result_matrix.get(2,2), 2);
}

TEST_F(Matrix_3x3, Inverse)
{
    matrix1.set({{-9,3,2}, {-2,1,1}, {2,2,2}});
    Matrix result_matrix = matrix1.inverse();

    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(0, 0)), 0);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(0, 1)), -0.3);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(0, 2)), 0.2);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(1, 0)), 1);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(1, 1)), -3.7);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(1, 2)), 0.8);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(2, 0)), -1);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(2, 1)), 4);
    EXPECT_EQ(Matrix_3x3::round_with_precision(result_matrix.get(2, 2)), -0.5);

    matrix1.set({{1,1,1}, {2,2,2},{10,10,10}});
    EXPECT_ANY_THROW(matrix1.inverse());
}

///============================================================================///

/// @brief Tests for random matrix
class Matrix_DIFFERENT : public ::testing::Test
{
protected:

    virtual void SetUp() {
        matrix1.set({{5,7,10,5}, {9,2,1,5}, {9,8,2,5}, {4,3,2,1}});
    }

    double round_with_precision( double val )
    {
        std::stringstream stringstream;
        stringstream << std::setprecision(1) << std::fixed << val;
        double new_val = stod(stringstream.str());
        return new_val;
    }

    Matrix matrix1{4,4};
};

TEST_F(Matrix_DIFFERENT, Constructor)
{
    EXPECT_EQ(matrix1.get(0,0), 5);
    EXPECT_EQ(matrix1.get(0,3), 5);
    EXPECT_EQ(matrix1.get(3,3), 1);

    EXPECT_ANY_THROW(Matrix m2(0,0));
}

TEST_F(Matrix_DIFFERENT, Setter)
{
    matrix1.set(3, 0, -9.5);
    EXPECT_EQ(matrix1.get(3, 0), -9.5);

    EXPECT_FALSE(matrix1.set(5, 0, -9.5));

    EXPECT_EQ(matrix1.get(0,0), 5);
    EXPECT_EQ(matrix1.get(1,2), 1);
    EXPECT_EQ(matrix1.get(2,3), 5);
    EXPECT_EQ(matrix1.get(3,3), 1);

    EXPECT_FALSE(matrix1.set({{3}}));
}

TEST_F(Matrix_DIFFERENT, Getter)
{
    matrix1.set(2, 2, -17);
    EXPECT_EQ(matrix1.get(2, 2), -17);
    EXPECT_ANY_THROW(matrix1.get(10, 1));
}

TEST_F(Matrix_DIFFERENT, EqualOperator)
{
    Matrix matrix2(4,4);
    matrix2.set({{5,7,10,5}, {9,2,1,5}, {9,8,2,5}, {4,3,2,1}});
    EXPECT_TRUE(matrix1 == matrix2);

    matrix1.set({{1,1,1,1}, {2,2,2,2}, {3,3,3,3}, {4,4,4,4}});
    EXPECT_FALSE(matrix1 == matrix2);

    Matrix matrix3(1,2);
    matrix3.set({{2,2}});
    EXPECT_ANY_THROW(matrix1 == matrix3);

    Matrix matrix1_3x2(3,2);
    Matrix matrix2_3x2(3,2);
    matrix1_3x2.set({{5,7}, {9,2}, {9,8}});
    matrix2_3x2.set({{5,7}, {9,2}, {9,8}});
    EXPECT_TRUE(matrix1_3x2 == matrix2_3x2);

    matrix2_3x2.set(1,1,7);
    EXPECT_FALSE(matrix1_3x2 == matrix2_3x2);
}

TEST_F(Matrix_DIFFERENT, SummOperator)
{
    Matrix matrix2(4, 4);
    matrix2.set({{5.2,7,10,1}, {9,2.3,1,0}, {9,8,2.8,6}, {1,1,1,1.1}});
    Matrix m_result = matrix1 + matrix2;
    EXPECT_EQ(Matrix_DIFFERENT::round_with_precision(m_result.get(0, 0)), 51.0 / 5);
    EXPECT_EQ(Matrix_DIFFERENT::round_with_precision(m_result.get(1, 1)), 43.0 / 10);
    EXPECT_EQ(Matrix_DIFFERENT::round_with_precision(m_result.get(2, 2)), 24.0 / 5);
    EXPECT_EQ(Matrix_DIFFERENT::round_with_precision(m_result.get(3, 3)), 21.0 / 10);
    EXPECT_EQ(m_result.get(0,3), 6);
    EXPECT_EQ(m_result.get(1,0), 18);
    EXPECT_EQ(m_result.get(2,1), 16);
    EXPECT_EQ(m_result.get(3,0), 5);
    EXPECT_EQ(m_result.get(3,2), 3);

    Matrix matrix3(2,1);
    EXPECT_ANY_THROW(matrix1 + matrix3);
}

TEST_F(Matrix_DIFFERENT, MultOperator)
{
    matrix1.set({{5.2,7,10,1}, {9,2.3,1,0}, {9,8,2.8,6}, {1,1,1,1.1}});
    Matrix m_result = matrix1 * (10);
    EXPECT_EQ(m_result.get(0,0), 52);
    EXPECT_EQ(m_result.get(1,1), 23);
    EXPECT_EQ(m_result.get(2,2), 28);
    EXPECT_EQ(m_result.get(3,3), 11);
    EXPECT_EQ(m_result.get(0,2), 100);
    EXPECT_EQ(m_result.get(1,3), 0);
    EXPECT_EQ(m_result.get(1,0), 90);
    EXPECT_EQ(m_result.get(2,3), 60);
    EXPECT_EQ(m_result.get(3,1), 10);
    EXPECT_EQ(m_result.get(3,2), 10);


    Matrix matrix2(3,2);
    matrix2.set({{1, 2}, {3, 4}, {5, 6}});

    Matrix matrix3(3,3);
    matrix3.set({{5,0,0}, {0,0,1}, {0,1,0}});

    m_result = matrix3 * matrix2;
    EXPECT_EQ(m_result.get(0, 0), 5);
    EXPECT_EQ(m_result.get(0, 1), 10);
    EXPECT_EQ(m_result.get(1, 0), 5);
    EXPECT_EQ(m_result.get(1, 1), 6);
    EXPECT_EQ(m_result.get(2, 0), 3);
    EXPECT_EQ(m_result.get(2, 1), 4);
    EXPECT_ANY_THROW(matrix2 * matrix1);
}

TEST_F(Matrix_DIFFERENT, SolveEquation)
{
    matrix1.set({{2,2,-3,1}, {1,2,4,2}, {-1,1,-1,1}, {1,-1,2,-2}});
    std::vector<double> m_result = matrix1.solveEquation({3,5,1,-4});

    EXPECT_EQ(m_result.at(0),1);
    EXPECT_EQ(m_result.at(1),-1);
    EXPECT_EQ(m_result.at(2),0);
    EXPECT_EQ(m_result.at(3),3);

    EXPECT_ANY_THROW(matrix1.solveEquation({1}));
    EXPECT_ANY_THROW(matrix1.solveEquation({1,2,3}));

    matrix1.set({{1.1, 1.1, 1.2}, {2.2, 2.2, 2.2},{10.10, 10.10, 10.10}});
    EXPECT_ANY_THROW(matrix1.solveEquation({1, 2, 3}));

    Matrix matrix2(1,2);
    EXPECT_ANY_THROW(matrix2.solveEquation({1,2}));
}

TEST_F(Matrix_DIFFERENT, Transpose)
{
    Matrix matrix2(3,2);
    matrix2.set({{1,2}, {3,4}, {5,6}});
    Matrix result_matrix = matrix2.transpose();
    EXPECT_EQ(result_matrix.get(0,0), 1);
    EXPECT_EQ(result_matrix.get(0,1), 3);
    EXPECT_EQ(result_matrix.get(0,2), 5);
    EXPECT_EQ(result_matrix.get(1,0), 2);
    EXPECT_EQ(result_matrix.get(1,1), 4);
    EXPECT_EQ(result_matrix.get(1,2), 6);
}

TEST_F(Matrix_DIFFERENT, Inverse)
{
    EXPECT_ANY_THROW(matrix1.inverse());

    Matrix matrix2(3,2);
    matrix2.set({{1,2}, {3,4}, {5,6}});
    EXPECT_ANY_THROW(matrix2.inverse());
}


/*** Konec souboru white_box_tests.cpp ***/
