//======== Copyright (c) 2017, FIT VUT Brno, All rights reserved. ============//
//
// Purpose:     Red-Black Tree - public interface tests
//
// $NoKeywords: $ivs_project_1 $black_box_tests.cpp
// $Author:     Georgii Troitckii <xtroit00@stud.fit.vutbr.cz>
// $Date:       $2022-02-22
//============================================================================//
/**
 * @file black_box_tests.cpp
 * @author Georgii Troitckii
 * 
 * @brief Implementace testu binarniho stromu.
 */

#include <vector>

#include "gtest/gtest.h"

#include "red_black_tree.h"

//============================================================================//
// ** ZDE DOPLNTE TESTY **
//
// Zde doplnte testy Red-Black Tree, testujte nasledujici:
// 1. Verejne rozhrani stromu
//    - InsertNode/DeleteNode a FindNode
//    - Chovani techto metod testuje pro prazdny i neprazdny strom.
// 2. Axiomy (tedy vzdy platne vlastnosti) Red-Black Tree:
//    - Vsechny listove uzly stromu jsou *VZDY* cerne.
//    - Kazdy cerveny uzel muze mit *POUZE* cerne potomky.
//    - Vsechny cesty od kazdeho listoveho uzlu ke koreni stromu obsahuji
//      *STEJNY* pocet cernych uzlu.
//============================================================================//

class EmptyTree : public ::testing::Test
{
protected:
    std::vector<int> keys = {0,-1,2,3,4,5,6,7,8,9,-10};

    BinaryTree tree;
};

TEST_F(EmptyTree, InsertNode_1)
{
    /// Insert first(root) node
    auto res_pair1 = tree.InsertNode(10);
    EXPECT_EQ(res_pair1.first, true);
    EXPECT_EQ(res_pair1.second->key, 10);
    EXPECT_EQ(res_pair1.second->color, BinaryTree::BLACK);
    EXPECT_EQ(res_pair1.second->pParent, nullptr);

    /// Check children nodes properties
    Node_t* left_leaf = res_pair1.second->pLeft;
    ASSERT_FALSE(left_leaf == nullptr);
    EXPECT_EQ(left_leaf->pLeft, nullptr);
    EXPECT_EQ(left_leaf->pRight, nullptr);
    EXPECT_EQ(left_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(left_leaf->pParent->color, BinaryTree::BLACK);
    EXPECT_EQ(left_leaf->pParent->key, 10);

    Node_t* right_leaf = res_pair1.second->pRight;
    ASSERT_FALSE(right_leaf == nullptr);
    EXPECT_EQ(right_leaf->pLeft, nullptr);
    EXPECT_EQ(right_leaf->pRight, nullptr);
    EXPECT_EQ(right_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(right_leaf->pParent->color, BinaryTree::BLACK);
    EXPECT_EQ(right_leaf->pParent->key, 10);
}

/// @brief Insert 3 nodes and check all it's connections
TEST_F(EmptyTree, InsertNode_2)
{
    auto res_pair1 = tree.InsertNode(10);
    ASSERT_FALSE(res_pair1.second == nullptr);

    /// Insert second node (value less than root node)
    auto res_pair2 = tree.InsertNode(7);
    ASSERT_FALSE(res_pair2.second == nullptr);

    EXPECT_EQ(res_pair2.first, true);
    EXPECT_EQ(res_pair2.second->key, 7);
    EXPECT_EQ(res_pair2.second->color, BinaryTree::RED);
    EXPECT_EQ(res_pair2.second->pParent, res_pair1.second);

    /// Check left node children properties
    Node_t* node2_left_leaf = res_pair2.second->pLeft;
    ASSERT_FALSE(node2_left_leaf == nullptr);
    EXPECT_EQ(node2_left_leaf->pLeft, nullptr);
    EXPECT_EQ(node2_left_leaf->pRight, nullptr);
    EXPECT_EQ(node2_left_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(node2_left_leaf->pParent->color, BinaryTree::RED);
    EXPECT_EQ(node2_left_leaf->pParent->key, 7);

    Node_t* node2_right_leaf = res_pair2.second->pRight;
    ASSERT_FALSE(node2_right_leaf == nullptr);
    EXPECT_EQ(node2_right_leaf->pLeft, nullptr);
    EXPECT_EQ(node2_right_leaf->pRight, nullptr);
    EXPECT_EQ(node2_right_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(node2_right_leaf->pParent->color, BinaryTree::RED);
    EXPECT_EQ(node2_right_leaf->pParent->key, 7);

    /// Check root right child properties
    Node_t* root_right_leaf = res_pair1.second->pRight;
    ASSERT_FALSE(root_right_leaf == nullptr);
    EXPECT_EQ(root_right_leaf->pLeft, nullptr);
    EXPECT_EQ(root_right_leaf->pRight, nullptr);
    EXPECT_EQ(root_right_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(root_right_leaf->pParent->color, BinaryTree::BLACK);
    EXPECT_EQ(root_right_leaf->pParent->key, 10);


    /// Insert third node (value more than root node)
    auto res_pair3 = tree.InsertNode(15);
    ASSERT_FALSE(res_pair3.second == nullptr);
    EXPECT_EQ(res_pair3.first, true);
    EXPECT_EQ(res_pair3.second->key, 15);
    EXPECT_EQ(res_pair3.second->color, BinaryTree::RED);
    EXPECT_EQ(res_pair3.second->pParent, res_pair1.second);

    /// Check right node children properties
    Node_t* node3_left_leaf = res_pair3.second->pLeft;
    ASSERT_FALSE(node3_left_leaf == nullptr);
    EXPECT_EQ(node3_left_leaf->pLeft, nullptr);
    EXPECT_EQ(node3_left_leaf->pRight, nullptr);
    EXPECT_EQ(node3_left_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(node3_left_leaf->pParent->color, BinaryTree::RED);
    EXPECT_EQ(node3_left_leaf->pParent->key, 15);

    Node_t* node3_right_leaf = res_pair3.second->pRight;
    ASSERT_FALSE(node3_right_leaf == nullptr);
    EXPECT_EQ(node3_right_leaf->pLeft, nullptr);
    EXPECT_EQ(node3_right_leaf->pRight, nullptr);
    EXPECT_EQ(node3_right_leaf->color, BinaryTree::BLACK);
    EXPECT_EQ(node3_right_leaf->pParent->color, BinaryTree::RED);
    EXPECT_EQ(node3_right_leaf->pParent->key, 15);

    /// Check root right child properties
    Node_t* root_right_node = res_pair1.second->pRight;
    ASSERT_FALSE(root_right_node == nullptr);
    EXPECT_EQ(root_right_node->color, BinaryTree::RED);
    EXPECT_EQ(root_right_node->pParent->color, BinaryTree::BLACK);
    EXPECT_EQ(root_right_node->pParent->key, 10);
}

/// @brief Insert existing node
TEST_F(EmptyTree, InsertNode_3)
{
    auto res_pair1 = tree.InsertNode(10);
    ASSERT_FALSE(res_pair1.second == nullptr);

    /// Insert second node (value less than root node)
    auto res_pair2 = tree.InsertNode(7);
    ASSERT_FALSE(res_pair2.second == nullptr);

    auto res_pair3 = tree.InsertNode(7);
    ASSERT_FALSE(res_pair3.second == nullptr);
    EXPECT_EQ(res_pair3.first, false);
    EXPECT_EQ(res_pair3.second, res_pair2.second);
}

/// @brief Insert different values
TEST_F(EmptyTree, InsertNode_4)
{
    auto res_pair1 = tree.InsertNode(-999);
    ASSERT_FALSE(res_pair1.second == nullptr);
    EXPECT_EQ(res_pair1.first, true);
    EXPECT_EQ(tree.GetRoot()->key, -999);
    EXPECT_EQ(res_pair1.second->color, BinaryTree::BLACK);

    auto res_pair2 = tree.InsertNode(0);
    ASSERT_FALSE(res_pair1.second == nullptr);
    EXPECT_EQ(res_pair2.first, true);
    EXPECT_EQ(res_pair2.second->key, 0);
    EXPECT_EQ(res_pair2.second->color, BinaryTree::RED);

    auto res_pair3 = tree.InsertNode(1234567);
    ASSERT_FALSE(res_pair3.second == nullptr);
    EXPECT_EQ(res_pair3.first, true);
    EXPECT_EQ(res_pair3.second->key, 1234567);
    EXPECT_EQ(res_pair3.second->color, BinaryTree::RED);

    auto root = tree.GetRoot();
    EXPECT_EQ(root->key, 0);
    EXPECT_EQ(root->pRight->key, 1234567);
    EXPECT_EQ(root->pLeft->key, -999);
}

TEST_F(EmptyTree, DeleteNode)
{
    for (int key : keys) {
        EXPECT_FALSE(tree.DeleteNode(key));
    }
}

TEST_F(EmptyTree, FindNode)
{
    for (int key : keys) {
        EXPECT_EQ(tree.FindNode(key), nullptr);
    }
}


class NonEmptyTree : public ::testing::Test
{
protected:
    virtual void SetUp() {
        for(int value : values)
            tree.InsertNode(value);
    }

    std::vector<int> values =  {8, 22, 26, 11, 10, 3, 18, 7, 4, 29, 1};
    BinaryTree tree;
};

/// @brief Inserting new node
TEST_F(NonEmptyTree, InsertNode_1)
{
    auto res_pair = tree.InsertNode(20);

    ASSERT_FALSE(res_pair.second == nullptr);
    EXPECT_EQ(res_pair.first, true);

    auto node = res_pair.second;
    EXPECT_EQ(node->color, BinaryTree::RED);
    EXPECT_EQ(node->key, 20);

    /// Check leaf nodes
    ASSERT_FALSE(node->pRight == nullptr);
    EXPECT_EQ(node->pRight->color, BinaryTree::BLACK);
    ASSERT_FALSE(node->pLeft == nullptr);
    EXPECT_EQ(node->pLeft->color, BinaryTree::BLACK);

    /// Check parent node
    auto parent_node = node->pParent;
    EXPECT_EQ(parent_node->color, BinaryTree::BLACK);
    EXPECT_EQ(parent_node->key, 18);
    EXPECT_EQ(parent_node->pLeft->key, 11);

}

/// @brief Inserting existing node
TEST_F(NonEmptyTree, InsertNode_2)
{
    /// Try to insert existing node
    auto result = tree.InsertNode(11);
    EXPECT_EQ(result.first, false);
    ASSERT_FALSE(result.second == nullptr);

    auto node = result.second;
    /// key, color; parent(key,color); left, right leaf
    EXPECT_EQ(node->color, BinaryTree::BLACK);
    EXPECT_EQ(node->key, 11);

    /// Check dependencies on parent node
    EXPECT_EQ(node->pParent->color, BinaryTree::RED);
    EXPECT_EQ(node->pParent->key, 22);
    ASSERT_EQ(node->pParent->pRight->key, 26);

    /// Check status of child nodes/leafs
    EXPECT_TRUE(node->pLeft != nullptr);
    EXPECT_EQ(node->pLeft->color, BinaryTree::BLACK);

    EXPECT_TRUE(node->pRight != nullptr);
    EXPECT_EQ(node->pRight->color, BinaryTree::RED);
    EXPECT_EQ(node->pRight->key, 18);
}

TEST_F(NonEmptyTree, DeleteNode)
{
    for (int node : values){
        EXPECT_TRUE(tree.DeleteNode(node));
    }

    for (int node : values){
        EXPECT_FALSE(tree.DeleteNode(node));
    }
}

TEST_F(NonEmptyTree, FindNode)
{
    for (int node : values) {
        EXPECT_TRUE(tree.FindNode(node) != nullptr);
    }

    tree.DeleteNode(10);
    EXPECT_TRUE(tree.FindNode(10) == nullptr);
}


class TreeAxioms : public ::testing::Test
{
protected:

    int countBlackNodesOnPath(Node_t* path_node)
    {
        int cnt_black_nodes = 0;

        while (path_node != nullptr){
            if (path_node->color == BinaryTree::BLACK){
                cnt_black_nodes++;
            }
            path_node = path_node->pParent;
        }

        return cnt_black_nodes;
    }

    BinaryTree tree;
};

/// @brief All NIL nodes are considered black.
TEST_F(TreeAxioms, Axiom1)
{
    std::vector<Node_t*> leafs;
    tree.GetLeafNodes(leafs);

    for (auto leaf : leafs) {
        ASSERT_FALSE(leaf->pLeft != nullptr);
        ASSERT_FALSE(leaf->pRight != nullptr);
        EXPECT_EQ(leaf->color, BinaryTree::BLACK);
    }
}

/// @brief A red node have only black children
TEST_F(TreeAxioms, Axiom2)
{
    std::vector<Node_t*> all_nodes;
    tree.GetAllNodes(all_nodes);

    for (auto node : all_nodes){
        if (node->color == BinaryTree::RED){
            EXPECT_EQ(node->pRight->color, BinaryTree::BLACK);
            EXPECT_EQ(node->pLeft->color, BinaryTree::BLACK);
        }
    }
}

/// @brief Every path from a given node to any of its descendant NIL nodes
///        goes through the same number of black nodes.
TEST_F(TreeAxioms, Axiom3)
{
    std::vector<Node_t*> leafs;
    tree.GetLeafNodes(leafs);

    /// Continue checking if the tree contains at least 1 element
    if (!leafs.empty()) {
        Node_t* first_path_node = leafs.at(0);
        int cnt_black_nodes = countBlackNodesOnPath(first_path_node);

        for (int i = 1; i < leafs.size(); ++i) {
            /// countBlackNodesOnPath - count black nodes
            ///                         on the path from NIL to root node
            EXPECT_EQ(countBlackNodesOnPath(leafs.at(i)), cnt_black_nodes);
        }
    }
}

/*** Konec souboru black_box_tests.cpp ***/
